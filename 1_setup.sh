#!/usr/bin/env bash

export KUBECONFIG="./exoscale-sks.kubeconfig"

echo ""
echo "••• Installing Longhorn storage controller •••"
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.1.1/deploy/longhorn.yaml

echo ""
echo "••• Installing K8up •••"
kubectl apply -f https://github.com/k8up-io/k8up/releases/download/v2.0.1/k8up-crd.yaml
kubectl create namespace k8up-operator
helm install k8up appuio/k8up --namespace k8up-operator

echo ""
echo "••• Done! Use kubectl or k9s to use your cluster •••"
