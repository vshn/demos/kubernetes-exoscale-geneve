#!/usr/bin/env bash

function k () {
    kubectl --kubeconfig exoscale-sks.kubeconfig "$@"
}

get_help()
{
   # Display Help
   echo "Get the GitLab configuration of an Exoscale cluster."
   echo
   echo "Syntax: ./0_configure.sh [-u|c|t|h]"
   echo "options:"
   echo "-k     Get the Exoscale SKS Kubeconfig file"
   echo "-u     Get the Exoscale SKS URL"
   echo "-c     Get the Exoscale SKS Certificate"
   echo "-t     Get the Exoscale SKS System account token"
   echo "-h     Print help"
   echo
}

get_kubeconfig()
{
    exo compute sks kubeconfig SKS-Cluster user -z ch-gva-2 --group system:masters > exoscale-sks.kubeconfig
}

get_url()
{
    # The `sed` command is used to remove colors in the `kubectl cluster-info` command output:
    # https://stackoverflow.com/a/51141872
    KUBERNETES_URL=$(k cluster-info | sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g' | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}')
    echo "$KUBERNETES_URL"
}

get_certificate()
{
    KUBERNETES_CERTIFICATE=$(k get secret $(k get secrets | grep default | awk '{ print $1 }') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
    echo "$KUBERNETES_CERTIFICATE"
}

get_token()
{
    k apply -f gitlab/service-account.yaml > /dev/null
    SYSTEM_ACCOUNT_TOKEN=$(k -n kube-system get secret -o jsonpath="{['data']['token']}" $(k -n kube-system get secret | grep gitlab | awk '{print $1}') | base64 --decode)
    echo "$SYSTEM_ACCOUNT_TOKEN"
}

while getopts ":huctk" option; do
    case $option in
        h) # display Help
            get_help
            exit;;
        u) # URL
            get_url
            exit;;
        c) # Certificate
            get_certificate
            exit;;
        t) # Token
            get_token
            exit;;
        k) # Kubeconfig
            get_kubeconfig
            exit;;
        \?) # Nothing
            echo "Error: Invalid option"
            exit;;
    esac
done
