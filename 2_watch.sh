#!/usr/bin/env bash

source ./environment.sh

# Watch how the number of snapshots grow
watch restic snapshots
