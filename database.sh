#!/usr/bin/env bash

DATABASE_NAME="wordpress"
REGION="ch-gva-2"

EXO_DB_JSON=$(exo dbaas show $DATABASE_NAME -z $REGION -O json)
EXO_DB_HOST=$(echo "$EXO_DB_JSON" | jq -r '.mysql.components[0].host')
EXO_DB_PORT=$(echo "$EXO_DB_JSON" | jq -r '.mysql.components[0].port')

echo "$EXO_DB_HOST:$EXO_DB_PORT"
